package dominio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Clase sistema Mantiene los datos del sistema (partidas jugadas, jugadores
 * existentes) Contiene y maneja la partida a jugar.
 *
 * @author Vincent Silva - 212677
 */
public class Sistema {

    private ArrayList<Jugador> jugadores;
    private ArrayList<Partida> partidas;
    private Partida partida;

    public Sistema() {
        this.partidas = new ArrayList<Partida>();
        this.jugadores = new ArrayList<Jugador>();
        this.jugadores.add(new Jugador("Pancho villa", "unJugRojo", 28));
        this.jugadores.add(new Jugador("Sancho panza", "unJugAzul", 27));
        this.jugadores.get(0).setPuntos(10);
        this.jugadores.get(1).setGanadas(10);
    }

    public ArrayList<Jugador> getJugadores() {
        return this.jugadores;
    }

    public Partida getPartida() {
        return this.partida;
    }

    public void setPartida(Partida partida) {
        this.partida = partida;
    }

    public ArrayList<Partida> getPartidas() {
        return this.partidas;
    }

    public void setPartidas(ArrayList<Partida> inPartidas) {
        this.partidas = inPartidas;
    }

    /**
     * Inicializa una partida nueva
     *
     * @param turnos turnos máximos de la partida
     * @param inJ1 jugador 1
     * @param inJ2 jugador 2
     */
    public void nuevaPartida(int turnos, int inJ1, int inJ2) {
        Jugador j1 = new Jugador(
                this.getJugadores().get(inJ1).getNombre(),
                this.getJugadores().get(inJ1).getAlias(),
                this.getJugadores().get(inJ1).getEdad()
        );

        Jugador j2 = new Jugador(
                this.getJugadores().get(inJ2).getNombre(),
                this.getJugadores().get(inJ2).getAlias(),
                this.getJugadores().get(inJ2).getEdad()
        );

        this.setPartida(new Partida(j1, j2, turnos));
        this.getPartida().nueva();
    }

    /**
     * Carga una partida con los valores preconfigurados
     */
    public void cargarPartida() {
        Jugador j1 = new Jugador(
                this.getJugadores().get(0).getNombre(),
                this.getJugadores().get(0).getAlias(),
                this.getJugadores().get(0).getEdad()
        );

        Jugador j2 = new Jugador(
                this.getJugadores().get(1).getNombre(),
                this.getJugadores().get(1).getAlias(),
                this.getJugadores().get(1).getEdad()
        );
        this.setPartida(new Partida(j1, j2, 6));
        int[][] pos1 = {{1, 4}, {1, 6}, {2, 1}, {3, 2}, {5, 1}, {5, 6}, {6, 4}, {6, 5}};
        int[][] pos2 = {{2, 2}, {3, 1}, {5, 3}, {6, 2}, {6, 6}, {7, 4}};
        for (int[] posicion : pos1) {
            this.getPartida().getTablero().setPieza(posicion, 1);
        }

        for (int[] posicion : pos2) {
            this.getPartida().getTablero().setPieza(posicion, 2);
        }

        this.getPartida().buscarMovimientos();
    }

    /**
     * Avanza los turnos y lleva el seguimiento de cuando se debe terminar la
     * partida
     *
     * @return True si es válido el avance de turno, False cuando no quedan
     * turnos para avanzar
     */
    public boolean siguienteTurno() {
        boolean respuesta = false;
        if (this.getPartida().getTurnos() > 1) {
            this.getPartida().siguienteTurno();
            this.getPartida().buscarMovimientos();
            respuesta = true;
        }
        return respuesta;
    }

    /**
     * Realiza el abandono de la partida actual por el jugador actual. Le
     * entrega la partida al otro jugador. Actualiza y almacena la partida y el
     * jugador en los registros.
     *
     * @return Jugador ganador de la partida.
     */
    public Jugador abandonar() {
        Jugador ganador = this.getPartida().getSigJugador();
        this.getPartidas().add(this.getPartida());
        for (Jugador jugador : this.getJugadores()) {
            if (ganador.equals(jugador)) {
                jugador.setGanadas(jugador.getGanadas() + 1);
            }
        }
        this.setPartida(null);
        return ganador;
    }

    /**
     * Realiza la terminación de la partida, entrega los puntos según el
     * triángulo mayor, setea el ganador, actualiza los registros de partida y
     * jugadores.
     *
     * @return Jugador ganador de la partida
     */
    public Jugador terminar() {
        double area1, area2;
        double areaMax = 0;
        int mult = 1;
        Jugador jugador1 = this.getPartida().getJugador1();
        Jugador jugador2 = this.getPartida().getJugador2();
        Jugador ganador = null;
        area1 = this.getPartida().trianguloMayor(jugador1);
        area2 = this.getPartida().trianguloMayor(jugador2);

        if (area1 > area2) {
            ganador = jugador1;
            areaMax = area1;
        } else if (area1 < area2) {
            ganador = jugador2;
            areaMax = area2;
        }

        if (areaMax >= 9) {
            mult = 3;
        } else if (areaMax >= 4) {
            mult = 2;
        }
        ganador.setPuntos(ganador.getPuntos() * mult);

        if (ganador != null) {
            this.getPartida().setGanador(ganador);
            for (Jugador jugador : this.getJugadores()) {
                if (jugador.equals(ganador)) {
                    jugador.setPuntos(jugador.getPuntos() + ganador.getPuntos());
                    jugador.setGanadas(jugador.getGanadas() + 1);
                }
            }
        }

        this.getPartidas().add(this.getPartida());
        this.setPartida(null);
        return ganador;
    }

    /**
     * Genera la lista de jugadores ordenada por el puntaje histórico
     *
     * @return
     */
    public ArrayList<Jugador> getRankingPuntaje() {
        ArrayList<Jugador> jugadores = this.getJugadores();
        Comparator<Jugador> comparator = new Comparator<Jugador>() {
            @Override
            public int compare(Jugador a, Jugador b) {
                return b.getPuntos() - a.getPuntos();
            }
        };

        Collections.sort(jugadores, comparator);

        return jugadores;
    }

    /**
     * Genera la lista de jugadores ordenada por la cantidad de partidas ganadas
     *
     * @return
     */
    public ArrayList<Jugador> getRankingPartidas() {
        ArrayList<Jugador> jugadores = this.getJugadores();
        Comparator<Jugador> comparator = new Comparator<Jugador>() {
            @Override
            public int compare(Jugador a, Jugador b) {
                return b.getGanadas() - a.getGanadas();
            }
        };

        Collections.sort(jugadores, comparator);
        return jugadores;
    }

    /**
     * Crea un nuevo jugador
     *
     * @param nombre
     * @param alias
     * @param edad
     * @return True si el jugador fué creado exitosamente, False si el jugador
     * está repetido
     */
    public boolean nuevoJugador(String nombre, String alias, int edad) {
        boolean respuesta = true;
        Jugador jugador = new Jugador(nombre, alias, edad);
        if (!existeJugador(jugador)) {
            this.getJugadores().add(jugador);
        } else {
            respuesta = false;
        }
        return respuesta;
    }

    /**
     * Busca el jugador en la lista de jugadores o si su alias está utilizado
     *
     * @param inJugador
     * @return True si existe, False si no
     */
    public boolean existeJugador(Jugador inJugador) {
        boolean respuesta = false;

        for (Jugador jugador : this.getJugadores()) {
            respuesta = respuesta
                    || jugador.equals(inJugador)
                    || jugador.getAlias().equals(inJugador.getAlias());
        }
        return respuesta;
    }
}
