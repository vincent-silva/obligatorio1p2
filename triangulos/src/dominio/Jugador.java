package dominio;

import java.util.Objects;

/**
 * Clase jugador. Mantiene sus propios datos personales, su puntaje dependiendo
 * del momento donde se esté instanciando. Y la cantidad de partidas ganadas
 * globalmente.
 *
 * @author Vincent
 */
public class Jugador implements Comparable<Jugador> {

    private String nombre;
    private String alias;
    private int edad;
    private int puntos;
    private int color;
    private int ganadas;

    public Jugador(String nombre, String alias, int edad) {
        this.nombre = nombre;
        this.alias = alias;
        this.edad = edad;
        this.puntos = 0;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getAlias() {
        return this.alias;
    }

    public int getEdad() {
        return this.edad;
    }

    public int getPuntos() {
        return this.puntos;
    }

    public void setPuntos(int inPuntos) {
        this.puntos = inPuntos;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getGanadas() {
        return this.ganadas;
    }

    public void setGanadas(int inGanadas) {
        this.ganadas = inGanadas;
    }

    /**
     * Se imprime a si mismo con el formato final nombre 'alias' (edad): x
     * Puntos x Ganadas
     *
     * @return
     */
    @Override
    public String toString() {
        return this.getNombre() + " '" + this.getAlias() + "' ("
                + this.getEdad() + " años): " + this.getPuntos() + " Puntos "
                + this.getGanadas() + " Ganadas";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.nombre);
        hash = 19 * hash + Objects.hashCode(this.alias);
        hash = 19 * hash + this.edad;
        return hash;
    }

    /**
     * Un jugador es igual a otro cuando todas sus propiedades son iguales
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Jugador other = (Jugador) obj;

        return this.getNombre().equals(other.getNombre())
                && this.getEdad() == other.getEdad()
                && this.getAlias().equals(other.getAlias());
    }

    /**
     * Por defecto los jugadores se ordenan por puntaje en forma ascendente
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(Jugador o) {
        return this.getPuntos() - o.getPuntos();
    }
}
