package dominio;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Clase tablero Mantiene los datos del tablero y realiza operaciones básicas
 * sobre si
 *
 * @author Vincent Silva - 212677
 */
public class Tablero {

    private static final int X_MAX = 7;
    private static final int Y_MAX = 7;
    private static final int VACIO = 0;
    private static final int ROJO = 1;
    private static final int AZUL = 2;
    private static final String COLOR_AZUL = "\u001B[34m";
    private static final String COLOR_ROJO = "\u001B[31m";
    private static final String COLOR_BLANCO = "\u001B[0m";

    private int[][] piezas;

    /**
     * Constructor de tablero
     */
    public Tablero() {
        this.piezas = new int[X_MAX + 1][Y_MAX + 1];
    }

    public int[][] getPiezas() {
        return piezas;
    }

    public void setPiezas(int[][] inPiezas) {
        this.piezas = inPiezas;
    }

    /**
     * Obtiene la lista de piezas relacionadas con un color en particular
     *
     * @param color Color del jugador a buscar
     * @return Lista de piezas
     */
    public ArrayList<int[]> getListaPiezas(int color) {
        ArrayList<int[]> respuesta = new ArrayList<>();
        for (int i = 1; i < this.piezas.length; i++) {
            for (int j = 1; j < this.piezas[0].length; j++) {
                if (this.piezas[i][j] == color) {
                    respuesta.add(new int[]{i, j});
                }
            }
        }
        return respuesta;
    }

    /**
     * Dados dos coordenadas devuelve la lista de posibles terceras coordenadas
     * que conforman triángulos válidos dentro del tablero.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return Lista de coordenadas de posibles vertices
     */
    public ArrayList<int[]> getVertices(int[] c1, int[] c2) {
        ArrayList<int[]> respuesta = new ArrayList<>();
        int[][] vertices = null;

        if (!Arrays.equals(c1, c2)) {
            if (this.esRecto(c1, c2)) {
                vertices = this.verticesRectos(c1, c2);
            } else if (this.esDiagonal(c1, c2)) {
                vertices = this.verticesDiagonales(c1, c2);
            }
        }

        if (vertices != null) {
            for (int[] vertice : vertices) {
                if (this.esPosValida(vertice)) {
                    respuesta.add(vertice);
                }
            }
        }
        return respuesta;
    }

    /**
     * Retorna el color de una pieza en una posición particular
     *
     * @param c1 Coordenada
     * @return Color de la pieza
     */
    public int getPieza(int[] c1) {
        return this.piezas[c1[0]][c1[1]];
    }

    /**
     * Setea el color de una pieza en una posicion en particular
     *
     * @param c1 Coordenada
     * @param color Color
     */
    public void setPieza(int[] c1, int color) {
        this.piezas[c1[0]][c1[1]] = color;
    }

    /**
     * Dadas tres coordenadas, determina si conforman un triángulo válido,
     * isósceles, rectángulo, cuya hipotenusa es o bien horizontal o diagonal
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @param c3 Coordenada 3
     * @return True si es un triángulo válido, False si no lo es.
     */
    public boolean esTriangulo(int[] c1, int[] c2, int[] c3) {
        boolean respuesta = false;
        int[][] tri = {c1, c2, c3, c1, c2, c1};

        for (int i = 0; i < 3; i++) {
            if (this.esRecto(tri[i], tri[i + 1])
                    && this.esDiagonal(tri[i], tri[i + 2])
                    && this.esDiagonal(tri[i + 1], tri[i + 2])) {
                respuesta = true;
                break;
            }
        }

        return respuesta;
    }

    /**
     * Determina si una coordenada es una posición válida, es decir, está dentro
     * del tablero y no la ocupa ninguna pieza.
     *
     * @param c1 Coordenada 1
     * @return True si es una posición válida, False si no lo es.
     */
    public boolean esPosValida(int[] c1) {
        return c1[0] > 0 && c1[0] <= X_MAX
                && c1[1] > 0 && c1[1] <= Y_MAX
                && (this.getPiezas()[c1[0]][c1[1]] == 0);
    }

    /**
     * Determina si un movimiento es válido, esto es si el trayecto desde la
     * primer coordenada hacia la segunda está libre.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return True si el movimiento es válido.
     */
    public boolean esMovValido(int[] c1, int[] c2) {
        int[][] tray;
        boolean valido = !Arrays.equals(c1, c2)
                && this.esPosValida(c2)
                && (this.esRecto(c1, c2) || this.esDiagonal(c1, c2));
        if (valido) {
            tray = this.trayecto(c1, c2);
            for (int[] tray1 : tray) {
                valido = valido && this.getPiezas()[tray1[0]][tray1[1]] == 0;
            }
        }

        return valido;
    }

    /**
     * Retorna el área de un trángulo dados 3 de sus coordenadas dada la fórmula
     * |(x1(y2 − y3) + x2(y3 − y1)+ x3(y1 − y2))|/2 que es el producto cruzado
     * de los dos vectores que forman el triangulo
     *
     * @param c1 Primer punto del triángulo
     * @param c2 Segundo punto del triángulo
     * @param c3 Tercer punto del triángulo
     * @return Área del triangulo
     */
    public double area(int[] c1, int[] c2, int[] c3) {
        return Math.abs((double) (c1[0] * (c2[1] - c3[1])
                + c2[0] * (c3[1] - c1[1])
                + c3[0] * (c1[1] - c2[1])) / 2);
    }

    /**
     * Chequea si un punto está dentro o no de un triangulo dados sus puntos
     *
     * @param c1 Primer punto del triángulo
     * @param c2 Segundo punto del triángulo
     * @param c3 Tercer punto del triángulo
     * @param c4 Punto a comprobar
     * @return True si el punto está dentro del triángulo
     */
    public boolean dentro(int[] c1, int[] c2, int[] c3, int[] c4) {
        return this.area(c1, c2, c3)
                == this.area(c4, c1, c2)
                + this.area(c4, c1, c3)
                + this.area(c4, c2, c3);
    }

    /**
     * Comprueba si dos coordenadas esán situadas verticalmente entre ellas
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return True si son verticales
     */
    private boolean esVertical(int[] c1, int[] c2) {
        return c1[0] == c2[0];
    }

    /**
     * Comprueba si dos coordenadas están situadas horizontalmente una de la
     * otra.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return True si son horizontales
     */
    private boolean esHorizontal(int[] c1, int[] c2) {
        return c1[1] == c2[1];
    }

    /**
     * Comprueba si dos coordenadas son horizontales o verticales.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return True si son rectas
     */
    private boolean esRecto(int[] c1, int[] c2) {
        return this.esHorizontal(c1, c2) || this.esVertical(c1, c2);
    }

    /**
     * Comprueba si dos coordendas están en la misma diagonal del tablero.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return True si son diagonales
     */
    private boolean esDiagonal(int[] c1, int[] c2) {
        return Math.abs(c1[0] - c2[0]) == Math.abs(c1[1] - c2[1]);
    }

    /**
     * Obtiene la distancia que separa dos coordenadas, o de otra forma, la
     * cantidad de pasos que se tienen que dar para llegar desde una coordenada
     * hacia la siguiente.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return Distancia entre las coordenadas
     */
    private int distancia(int[] c1, int[] c2) {
        return Math.max(Math.abs(c1[1] - c2[1]), Math.abs(c1[0] - c2[0]));
    }

    /**
     * Calcula la pentiente que forman dos coordenadas diagonales.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return Número positivo si su pendiente es positiva, negativo en caso
     * contrario
     */
    private int pendiente(int[] c1, int[] c2) {
        return (c2[1] - c1[1]) / (c2[0] - c1[0]);
    }

    /**
     * Obtiene la lista de coordenadas entre dos coordenadas
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return Lista de coordenadas
     */
    private int[][] trayecto(int[] c1, int[] c2) {
        int dist = this.distancia(c1, c2);
        int sumaX = (c2[0] - c1[0]) / dist;
        int sumaY = (c2[1] - c1[1]) / dist;
        int[][] respuesta = new int[dist][2];

        for (int i = 0; i < dist; i++) {
            respuesta[i][0] = c1[0] + sumaX * (i + 1);
            respuesta[i][1] = c1[1] + sumaY * (i + 1);
        }

        return respuesta;
    }

    /**
     * Obtiene los vertices posibles para las coordenadas que son rectas entre
     * si
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return Lista de vértices posibles
     */
    private int[][] verticesRectos(int[] c1, int[] c2) {
        int[][] respuesta = null;
        int dist = this.distancia(c1, c2);
        int a = Math.min(c1[0], c2[0]);
        int b = c1[1];

        if (this.esVertical(c1, c2)) {
            a = Math.min(c1[1], c2[1]);
            b = c1[0];
        }

        if (dist % 2 == 0 && dist < Math.min(X_MAX, Y_MAX)) {
            respuesta = new int[][]{
                {a + (dist / 2), b + (dist / 2)},
                {a + (dist / 2), b - (dist / 2)}
            };
        }

        return respuesta;
    }

    /**
     * Obtiene la lista de vértices posibles para dos cordenadas diagonales
     * entre si.
     *
     * @param c1 Coordenada 1
     * @param c2 Coordenada 2
     * @return Lista de vértices
     */
    private int[][] verticesDiagonales(int[] c1, int[] c2) {
        int x = c1[0];
        int y = c1[1];
        int d = this.distancia(c1, c2);
        int p = this.pendiente(c1, c2);
        //Ordena puntos por su x
        if (c2[0] < c1[0]) {
            x = c2[0];
            y = c2[1];
        }

        return new int[][]{
            {x - d, y + (d * p)},
            {x + (d * p), y - d},
            {x, y + (2 * d * p)},
            {x + (2 * d), y}
        };
    }

    @Override
    public String toString() {
        String response = "";
        String fila;
        String separador;

        for (int y = Y_MAX; y > 0; y--) {
            fila = "|";
            separador = "+-+-+-+-+-+-+-+";

            if (y == Y_MAX || y == 1) {
                fila = "  |";
            }

            if (y == Y_MAX) {
                separador = "  +-+-+-+-+-+";
            }

            for (int x = 1; x <= X_MAX; x++) {
                //Si no estamos ni en la primera ni ultima fila
                // ni estamos en la primera o ultima celda de cada fila
                if ((x != X_MAX && x != 1) || (y != Y_MAX && y != 1)) {
                    switch (this.getPiezas()[x][y]) {
                        case ROJO:
                            fila += COLOR_ROJO + "■" + COLOR_BLANCO + "|";
                            break;
                        case AZUL:
                            fila += COLOR_AZUL + "@" + COLOR_BLANCO + "|";
                            break;
                        default:
                            fila += " |";
                    }
                }
            }

            response += "  " + separador + "\r\n" + y + " " + fila + "\r\n";
        }

        response += "    +-+-+-+-+-+\r\n   A B C D E F G";

        return response;
    }
}
