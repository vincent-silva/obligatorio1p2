package dominio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Clase partida. Mantiene sus propios datos (constantes de color para
 * jugadores, ganador de la partida, turnos restantes, movimientos a realizar
 * cada turno). Contiene y maneja el tablero y a los jugadores
 *
 * @author Vincent Silva - 212677
 */
public class Partida {

    private static final int VACIO = 0;
    private static final int ROJO = 1;
    private static final int AZUL = 2;
    private static final int FICHAS_INICIALES = 17;
    private int turnos;
    private Jugador jugador1;
    private Jugador jugador2;
    private Jugador jugador;
    private Tablero tablero;
    private ArrayList<int[][]> movimientos;
    private Object ganador;

    public Partida(Jugador inJ1, Jugador inJ2, int inTurnos) {
        this.jugador1 = inJ1;
        this.jugador2 = inJ2;
        this.jugador1.setColor(ROJO);
        this.jugador2.setColor(AZUL);
        this.jugador = this.jugador1;
        this.turnos = inTurnos;
        this.tablero = new Tablero();
        this.movimientos = new ArrayList<>();
        this.ganador = null;
    }

    public int getTurnos() {
        return turnos;
    }

    public void setTurnos(int turnos) {
        this.turnos = turnos;
    }

    /**
     * Retorna el que tiene el turno de jugar
     *
     * @return
     */
    public Jugador getJugador() {
        return jugador;
    }

    /**
     * Devuelve el proximo jugador a mover
     *
     * @return
     */
    public Jugador getSigJugador() {
        Jugador respuesta;
        if (this.jugador.equals(this.jugador1)) {
            respuesta = this.jugador2;
        } else {
            respuesta = this.jugador1;
        }
        return respuesta;
    }

    /**
     * Setea el jugador actual
     *
     * @param jugador
     */
    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Jugador getJugador1() {
        return jugador1;
    }

    public void setJugador1(Jugador jugador1) {
        this.jugador1 = jugador1;
    }

    public Jugador getJugador2() {
        return jugador2;
    }

    public void setJugador2(Jugador jugador2) {
        this.jugador2 = jugador2;
    }

    public Tablero getTablero() {
        return tablero;
    }

    public void setTablero(Tablero tablero) {
        this.tablero = tablero;
    }

    /**
     * Devuelve la lista de movimientos posibles para el turno
     *
     * @return
     */
    public ArrayList<int[][]> getMovimientos() {
        return movimientos;
    }

    /**
     * Setea la lista de movimientos posibles
     *
     * @param movimientos
     */
    public void setMovimientos(ArrayList<int[][]> movimientos) {
        this.movimientos = movimientos;
    }

    public void setGanador(Jugador inJugador) {
        this.ganador = inJugador;
    }

    /**
     * Inicializa la partida, rellena el tablero con las fichas segun las
     * constantes definidas
     */
    public void nueva() {
        Random gen = new Random();
        int[] pos = new int[2];
        int color = ROJO;
        for (int i = 0; i < FICHAS_INICIALES * 2; i++) {
            if (i >= FICHAS_INICIALES) {
                color = AZUL;
            }

            do {
                pos[0] = gen.nextInt(7) + 1;
                pos[1] = gen.nextInt(7) + 1;
            } while (!this.getTablero().esPosValida(pos));

            this.getTablero().setPieza(pos, color);
        }

        this.buscarMovimientos();
    }

    /**
     * Avanza al siguiente turno
     */
    public void siguienteTurno() {
        this.setJugador(this.getSigJugador());
        this.setTurnos(this.getTurnos() - 1);
    }

    /**
     * Realiza un movimiento, recibe las coordenadas inicial y final. Además de
     * los vértices (en cualquier orden)
     *
     * @param c1 Coordenada original
     * @param c2 Coordenada destino
     * @param t1 Vértice del triángulo
     * @param t2 Vértice del triángulo
     * @return True si el movimiento fué exitoso, false si el movimiento o el
     * triángulo formado no es válido
     */
    public boolean mover(int[] c1, int[] c2, int t1[], int t2[]) {
        boolean response = false;
        int colorActual = this.getJugador().getColor();
        if (colorActual == this.getTablero().getPieza(c1)
                && colorActual == this.getTablero().getPieza(t1)
                && colorActual == this.getTablero().getPieza(t2)
                && (this.getTablero().esMovValido(c1, c2)
                && this.getTablero().esTriangulo(c2, t1, t2))) {
            this.getTablero().setPieza(c1, VACIO);
            this.getTablero().setPieza(c2, colorActual);
            response = true;
        }
        return response;
    }

    /**
     * Realiza la captura de todas las piezas dentro del área de un triángulo, a
     * excepción de los vértices mismos. Suma al subtotal de puntos del jugador
     * la cantidad de piezas contrarias capturadas de esta manera.
     *
     * @param c1 Vértice 1
     * @param c2 Vértice 2
     * @param c3 Vértice 3
     * @return Lista de piezas removidas del tablero
     */
    public ArrayList<int[]> capturar(int[] c1, int[] c2, int[] c3) {
        ArrayList<int[]> removidos = new ArrayList<>();
        ArrayList<int[]> piezas;
        int puntos = this.getJugador().getPuntos();

        piezas = this.getTablero().getListaPiezas(ROJO);
        piezas.addAll(this.getTablero().getListaPiezas(AZUL));

        for (int i = 0; i < piezas.size(); i++) {
            if (!Arrays.equals(piezas.get(i), c1)
                    && !Arrays.equals(piezas.get(i), c2)
                    && !Arrays.equals(piezas.get(i), c3)) {

                if (this.getTablero().dentro(c1, c2, c3, piezas.get(i))) {
                    if (this.getJugador().getColor()
                            != this.tablero.getPieza(piezas.get(i))) {
                        puntos++;
                    }
                    this.tablero.setPieza(piezas.get(i), VACIO);
                    removidos.add(piezas.get(i));
                }
            }
        }

        this.jugador.setPuntos(puntos);
        return removidos;
    }

    /**
     * Busca todos los movimientos que puede realizar el jugador en este turno,
     * con las piezas disponibles. Almacena la búsqueda resultante en la lista
     * de movimientos.
     */
    public void buscarMovimientos() {
        int[] p1, p2;
        ArrayList<int[]> piezas
                = this.getTablero().getListaPiezas(this.getJugador().getColor());
        ArrayList<int[]> vertices = new ArrayList<>();
        ArrayList<int[][]> mov = new ArrayList<>();

        this.getMovimientos().clear();

        for (int i = 0; i < piezas.size(); i++) {
            for (int j = i + 1; j < piezas.size(); j++) {
                p1 = piezas.get(i);
                p2 = piezas.get(j);
                vertices = this.getTablero().getVertices(p1, p2);

                if (!vertices.isEmpty()) {
                    mov.addAll(this.movPosibles(p1, p2, vertices, piezas));
                }
            }
        }

        if (!mov.isEmpty()) {
            this.getMovimientos().addAll(mov);
        }
    }

    /**
     * Devuelve la lista de movimientos posibles para dos cordenadas, una lísta
     * de posiciones candidatas a vértices y la lista de piezas a mover.
     *
     * @param p1 Vértice 1
     * @param p2 Vértice 2
     * @param vertices Posibles vértices 3
     * @param piezas Piezas restantes
     * @return Lista de los movimientos posibles dadas dos coordenadas en el
     * tablero. Lista vacía si no existen movimientos disponibles.
     */
    public ArrayList<int[][]> movPosibles(
            int[] p1, 
            int[] p2, 
            ArrayList<int[]> vertices, 
            ArrayList<int[]> piezas) {
        ArrayList<int[][]> respuesta = new ArrayList<>();

        for (int[] vertice : vertices) {
            for (int[] pieza : piezas) {
                if (!Arrays.equals(p1, vertice)
                        && !Arrays.equals(p1, pieza)
                        && !Arrays.equals(p2, vertice)
                        && !Arrays.equals(p2, pieza)) {
                    if (this.getTablero().esMovValido(pieza, vertice)) {
                        respuesta.add(new int[][]{pieza, vertice, p1, p2});
                    }
                }
            }
        }

        return respuesta;
    }

    /**
     * Busca el triángulo de mayor área para un jugador
     *
     * @param jugador Jugador a buscarle el área
     * @return El área del mayor triángulo para el jugador o 0.
     */
    public double trianguloMayor(Jugador jugador) {
        ArrayList<int[]> piezas = 
                this.getTablero().getListaPiezas(jugador.getColor());
        double areaMayor = 0;
        double area = 0;

        for (int i = 0; i < piezas.size(); i++) {
            for (int j = i + 1; j < piezas.size(); j++) {
                for (int k = j + 1; k < piezas.size(); k++) {
                    if (this.getTablero().esTriangulo(
                            piezas.get(i), piezas.get(j), piezas.get(k))) {
                        area = this.getTablero().area(
                                piezas.get(i), piezas.get(j), piezas.get(k));
                        if (areaMayor < area) {
                            areaMayor = area;
                        }
                    }
                }
            }
        }

        return areaMayor;
    }

    /**
     * Busca si existen movimientos posibles
     *
     * @return Retorna true si no hay movimientos posibles, eso es cuando la
     * lista de movimientos está vacía
     */
    public boolean hayMovimientos() {
        return !this.getMovimientos().isEmpty();
    }

    /**
     * Busca los movimientos posibles dada una coordenada destino
     *
     * @param c1 Cordenada a la que se quiere mover
     * @return Lista de movimientos posibles, o una lista vacíía de no existir.
     */
    public ArrayList<int[][]> ayudaPosicion(int[] c1) {
        ArrayList<int[][]> respuesta = new ArrayList<>();

        for (int[][] movimiento : this.getMovimientos()) {
            if (Arrays.equals(movimiento[1], c1)) {
                respuesta.add(movimiento);
            }
        }

        return respuesta;
    }

    @Override
    public String toString() {
        return this.jugador1.getAlias() + ": " + this.jugador1.getPuntos() + " "
                + this.jugador2.getAlias() + ": " + this.jugador2.getPuntos()
                + "\r\n" + this.tablero.toString()
                + "\r\nJuega: " + this.getJugador().getAlias();
    }
}
