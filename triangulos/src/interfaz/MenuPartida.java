package interfaz;

import dominio.Jugador;
import dominio.Sistema;
import java.util.ArrayList;

/**
 * Pantalla donde se juega triángulos.
 *
 * @author Vincent Silva - 212677
 */
public final class MenuPartida {
    private Lector lector;
    private Sistema sistema;

    public MenuPartida(Sistema inSistema) {
        this.lector = new Lector();
        this.sistema = inSistema;
        menuNuevaPartida();
    }

    public Sistema getSistema() {
        return sistema;
    }

    /**
     * Menú para crear la partida nueva
     */
    public void menuNuevaPartida() {
        Sistema modelo = this.getSistema();
        int turnosMax = this.lector.enteroPositivo(
                "Ingrese la cantidad de turnos por partida: ");
        int j1 = elegirJugador(1);
        int j2 = elegirJugador(2);
        modelo.nuevaPartida(turnosMax, j1, j2);
//        modelo.cargarPartida();
        this.menu();
    }

    /**
     * Menú principal de la partida
     */
    public void menu() {
        Sistema modelo = this.getSistema();
        String in = "";
        while (modelo.getPartida() != null) {
            System.out.println("\r\n");
            System.out.println(modelo.getPartida().toString());
            System.out.println("Quedan: "+modelo.getPartida().getTurnos()
                    +" turnos");
            if (this.movimientosDisponibles()) {
                in = this.lector.texto("Movimiento (H para ayuda): ",
                        "^([a-zA-Z]\\d{1,2}\\s*){2}|[YFXTH]$");
            } else {
                in = this.lector.texto("No le quedan más movimientos:",
                        "^[YFXHT]$");
            }

            switch (in) {
                case "H":
                    this.mostrarAyuda();
                    break;
                case "Y":
                    this.ayuda();
                    break;
                case "X":
                    this.abandonar();
                    break;
                case "T":
                    this.pasarTurno();
                    break;
                case "F":
                    this.terminar();
                    break;
                default:
                    this.moverPieza(in);
            }
        }

    }

    /**
     * Muestra la ayuda del juego
     */
    public void mostrarAyuda() {
        System.out.println("\r\n");
        System.out.println("Y: Ayuda");
        System.out.println("X: Abandonar partida");
        System.out.println("T: Pasar el turno");
        System.out.println("F: Terminar la partida");
    }

    /**
     * Busca si existen movimientos disponibles para el turno
     *
     * @return True si existen, False si no
     */
    private boolean movimientosDisponibles() {
        return this.getSistema().getPartida().hayMovimientos();
    }

    /**
     * Menú para el acceso a la ayuda para una posición
     */
    private void ayuda() {
        Sistema modelo = this.getSistema();
        ArrayList<int[][]> movimientos;
        String in = this.lector.listaCoordenadas(
                "Sobre que posición necesita ayuda?", 1);
        movimientos = modelo.getPartida().ayudaPosicion(this.strAPosicion(in));
        if (!movimientos.isEmpty()) {
            System.out.println("Para este casillero "
                    + "tiene los siguientes movimientos:");

            for (int[][] movimiento : movimientos) {
                System.out.println(this.posicionAStr(movimiento[0])
                        + " => " + this.posicionAStr(movimiento[1])
                        + " " + this.posicionAStr(movimiento[2])
                        + ":" + this.posicionAStr(movimiento[3]));
            }
        } else {
            System.out.println(
                    "Para esta posición no tiene movimientos disponibles");
        }
    }

    /**
     * Menú para abandonar la partida
     */
    private void abandonar() {
        Sistema modelo = this.getSistema();
        Jugador ganador = modelo.abandonar();
        System.out.println("\r\nGana la partida: "
                + ganador.getAlias() + " por abandono");
    }

    /**
     * Menu para finalizar la partida
     */
    private void terminar() {
        Sistema modelo = this.getSistema();
        Jugador ganador = modelo.terminar();
        if (ganador != null) {
            System.out.println("\r\nGana la partida: "
                    + ganador.getAlias()+" con "+ganador.getPuntos()+" puntos");
        } else {
            System.out.println("\r\nLa partida termina en empate");
        }
    }

    /**
     * Menú que controla el movimiento de una pieza, realizar la validación del
     * mismo y pasar de turno
     *
     * @param pos
     */
    private void moverPieza(String pos) {
        Sistema modelo = this.getSistema();
        int[] iniCoord, finCoord, iniTri, finTri;
        ArrayList<int[]> listaRem;
        String removidas = "";

        String in = this.lector.listaCoordenadas(
                "Ingrese las coordenadas del triangulo", 2);
        iniCoord = strAPosicion(pos.split(" ")[0]);
        finCoord = strAPosicion(pos.split(" ")[1]);
        iniTri = strAPosicion(in.split(" ")[0]);
        finTri = strAPosicion(in.split(" ")[1]);

        if (modelo.getPartida().mover(iniCoord, finCoord, iniTri, finTri)) {
            listaRem = modelo.getPartida().capturar(finCoord, iniTri, finTri);
            for (int i = 0; i < listaRem.size(); i++) {
                removidas = removidas + " " + posicionAStr(listaRem.get(i));
            }
            System.out.println("Se remueven las siguientes fichas: "+removidas);
            this.siguienteTurno();
        } else {
            System.out.println("Movimiento no válido");

        }
    }

    /**
     * Pasa de turno, si no es posible, solicita terminar la partida.
     */
    private void siguienteTurno() {
        Sistema modelo = this.getSistema();
        if (!modelo.siguienteTurno()) {
            this.terminar();
        }
    }

    /**
     * Pasa el turno actual si no existen movimientos disponibles
     */
    private void pasarTurno() {
        Sistema modelo = this.getSistema();
        if (modelo.getPartida().hayMovimientos()) {
            System.out.println("\r\nNo se puede pasar el turno, "
                    + "todavía hay movimientos disponibles");
        } else {
            this.siguienteTurno();
        }
    }

    /**
     * Menu para la elección de jugador a jugar
     *
     * @param num Numero del jugador puede ser 1 o 2
     * @return el id del jugador
     */
    private int elegirJugador(int num) {
        Sistema modelo = this.sistema;
        System.out.println("Eliga jugador " + num + ":");
        for (int i = 0; i < modelo.getJugadores().size(); i++) {
            System.out.println(i + ": " + 
                    modelo.getJugadores().get(i).toString());
        }
        return this.lector.enteroRango(
                "Jugador: ", 0, modelo.getJugadores().size()
        );
    }

    /**
     * Convierte una cadena de caracteres validada como coordenada en una
     * coordenada numérica
     *
     * @param inPos Cordenada en forma de string
     * @return Array con coordenada
     */
    public int[] strAPosicion(String inPos) {
        int x = (inPos.toUpperCase().charAt(0) - 64);
        int y = Integer.parseInt(inPos.substring(1));
        return new int[]{x, y};
    }

    /**
     * Convierte un array con dos enteros en una coordenada en forma de string
     *
     * @param inPos Coordenada en forma de array
     * @return String con la coordenada
     */
    public String posicionAStr(int[] inPos) {
        return "" + ((char) (inPos[0] + 64)) + inPos[1];
    }
}
