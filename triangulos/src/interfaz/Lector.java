package interfaz;

import java.util.Scanner;

/**
 * Clase lector
 * Se encarga de leer datos de consola y devolverlos
 *
 * @author Vincent Silva - 212677
 */
public class Lector {

    public Lector(){
        
    }
    
    /**
     * Lee un entero, vuelve a pedirlo en caso de estar incorrecto
     *
     * @param mensaje Mensaje a mostrar
     * @return El entero ingresado por el usuario
     */
    public int entero(String mensaje) {
        Scanner in = new Scanner(System.in);
        boolean correcto = false;
        int dato = 0;
        while (!correcto) {
            try {
                System.out.println(mensaje);
                dato = in.nextInt();
                correcto = !correcto;
            } catch (Exception e) {
                System.out.println("El dato no es correcto");
                in.nextLine();
            }
        }

        return dato;
    }

    /**
     * Solicita un entero mayor a 0
     *
     * @param mensaje mensaje a mostrar
     * @return El dato ingresado por el usuario
     */
    public int enteroPositivo(String mensaje) {
        Scanner in = new Scanner(System.in);
        boolean correcto = false;
        int dato = -1;
        while (!correcto) {
            dato = this.entero(mensaje);
            if (dato < 1) {
                System.out.println("El número debe ser positivo");
            } else {
                correcto = true;
            }
        }

        return dato;
    }

    /**
     * Solicita un entero dentro del rango ingresado
     *
     * @param mensaje
     * @param min
     * @param max
     * @return
     */
    public int enteroRango(String mensaje, int min, int max) {
        Scanner in = new Scanner(System.in);
        boolean correcto = false;
        int dato = 0;
        while (!correcto) {
            dato = this.entero(mensaje);
            if (dato < min || dato > max) {
                System.out.println("El dato debe estar entre " + min + " y "
                        + max);
            } else {
                correcto = true;
            }
        }

        return dato;
    }

    /**
     * *
     * Solicita una cadena de caracteres, debe cumplir con la expresión regular
     * recibida
     *
     * @param mensaje Mensaje a mostrar
     * @param regex Expresión regular a evaluar
     * @return El dato ingresado
     */
    public String texto(String mensaje, String regex) {
        Scanner in = new Scanner(System.in);
        boolean correcto = false;
        String dato = "";

        while (!correcto) {
            System.out.println(mensaje);
            dato = in.nextLine();
            correcto = dato.trim().matches(regex);

            if (!correcto) {
                System.out.println("No se admiten caracteres inválidos");
            }
        }

        return dato;
    }

    /**
     * Solicita una cadena de caracteres que solo puede contener palábras de
     * caracteres válidos
     *
     * @param mensaje Mensaje a mostrar
     * @return El dato ingresado
     */
    public String texto(String mensaje) {
        return this.texto(mensaje, "^[\\p{L}\\s]+$");
    }

    /**
     * Solicita una coordenada, compuesta por una letra seguida por un número
     *
     * @param mensaje Mensaje a mostrar
     * @return El dato ingresado
     */
    public String coordenada(String mensaje) {
        return this.texto(mensaje, "^[a-zA-Z]\\d{1,2}$");
    }

    /**
     * Solicita una lista de coordenadas, compuestas por letras seguidas de
     * números y separadas por espacios. Deben ser una cantidad fija.
     *
     * @param mensaje Mensaje a mostrar
     * @param tam Tamaño de la lista
     * @return El dato ingresado
     */
    public String listaCoordenadas(String mensaje, int tam) {
        return this.texto(mensaje, "^([a-zA-Z]\\d{1,2}\\s*){" + tam + "}$");
    }

    /**
     * Muestra una pregunta en pantalla y retorna la respuesta en forma numérica
     *
     * @param mensaje El mensaje a mostrar
     * @return 1 si la respuesta es afirmativa, 2 si es negativa
     */
    public boolean pregunta(String mensaje) {
        Scanner in = new Scanner(System.in);
        boolean correcto = false;
        boolean dato = false;
        while (!correcto) {
            System.out.println(mensaje);
            switch (in.nextLine().toLowerCase()) {
                case "si":
                case "s":
                case "1":
                    dato = true;
                    correcto = true;
                    break;

                case "no":
                case "n":
                case "0":
                    correcto = false;
                    break;

                default:
                    System.out.println("Opcion incorrecta");
            }
        }
        return dato;
    }
}
