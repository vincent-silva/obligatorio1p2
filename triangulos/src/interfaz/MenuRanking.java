package interfaz;

import dominio.Jugador;
import dominio.Sistema;
import java.util.ArrayList;

/**
 * Pantalla de ranking de usuarios
 *
 * @author Vincent Silva - 212677
 */
public class MenuRanking {
    private Lector lector;
    private Sistema sistema;

    MenuRanking(Sistema inSistema) {
        this.lector = new Lector();
        this.sistema = inSistema;
        this.menu();
    }

    public Sistema getSistema() {
        return this.sistema;
    }

    public void setSistema(Sistema inSistema) {
        this.sistema = inSistema;
    }

    /**
     * Muestra el menu que permite elegir entre ordenar por partidas ganadas o
     * por puntaje de cada jugador
     */
    public void menu() {
        int op = -1;
        while (op != 0) {
            System.out.println("\r\n");
            System.out.println("1. Mostrar por partidas ganadas");
            System.out.println("2. Mostrar por puntos adquiridos");
            System.out.println("0. Salir");
            op = this.lector.entero("Ingrese una opción: ");

            switch (op) {
                case 1:
                    this.porPartidas();
                    break;

                case 2:
                    this.porPuntos();
                    break;

                case 0:
                    break;

                default:
                    System.out.println("Opcion incorrecta");
            }
        }
    }

    /**
     * Muestra el ranking ordenado por partidas ganadas
     */
    private void porPartidas() {
        this.mostrarRanking(this.sistema.getRankingPartidas());
    }

    /**
     * Muestra el ranking ordenado por puntos obtenidos
     */
    private void porPuntos() {
        this.mostrarRanking(this.sistema.getRankingPuntaje());
    }

    /**
     * Muestra una lista de jugadores
     *
     * @param jugadores Lista de jugadores
     */
    private void mostrarRanking(ArrayList<Jugador> jugadores) {
        System.out.println("\r\n");
        for (int i = 0; i < jugadores.size(); i++) {
            System.out.printf("1: ");
            System.out.println(jugadores.get(i).toString());
        }
    }
}
