package interfaz;

import dominio.Sistema;

/**
 * Pantalla de creación de jugadores
 * @author Vincent Silva - 212677
 */
public class MenuJugadores {
    private Sistema sistema;
    private Lector lector;
    
    public MenuJugadores(Sistema inModelo) {
        this.lector = new Lector();
        this.sistema  = inModelo;
        this.menu();
    }
    
    public void menu() {
        int op = -1;
        while (op != 0) {
            System.out.println("1. Crear jugador");
            System.out.println("2. Mostrar jugadores");
            System.out.println("0. Salir");
            op = this.lector.entero("Ingrese una opción: ");

            switch (op) {
                case 1:
                    this.crear();
                    break;
                    
                case 2:
                    this.listar();
                    break;
                    
                case 0:
                    break;
                    
                default:
                    System.out.println("Opcion incorrecta");
            }
        }
    }
    
    /**
     * Lista los jugadores creados en el sistema
     */
    public void listar(){
        System.out.println("Lista de jugadores del sistema:");
        Sistema modelo = this.sistema;
        for (int i = 0; i < modelo.getJugadores().size() ; i++) {
            System.out.println(modelo.getJugadores().get(i).toString());
        }
    }
    
    /**
     * Crea un nuevo jugador
     */
    public void crear() {
        Sistema modelo = this.sistema;
        String nombre = this.lector.texto("Ingrese el nombre del jugador: ");
        String alias = this.lector.texto("Ingrese el alias para el jugador: ");
        int edad = this.lector.enteroRango("Ingrese la edad para el jugador: ",1,150);

        if (!modelo.nuevoJugador(nombre, alias, edad)){
            System.out.println("El jugador ingresado ya existe!");
        }
    }
}
