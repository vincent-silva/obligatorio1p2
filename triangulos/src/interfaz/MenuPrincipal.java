package interfaz;


import dominio.Sistema;

/**
 * Pantalla de menú principal del juego
 * @author Vincent Silva - 212677
 */
public class MenuPrincipal{
    private Sistema modelo;
    private Lector lector;
    
    public MenuPrincipal() {
        this.modelo = new Sistema();
        this.lector = new Lector();
        this.menu();
    }
    
    
    /**
     * Muestra el menú principal 
     */
    private void menu(){
        int op = -1;
        while (op != 0) {
            System.out.println("Menu principal");
            System.out.println("1. Nueva partida");
            System.out.println("2. Ranking");
            System.out.println("3. Jugadores");
            System.out.println("0. Salir");
            op = this.lector.entero("Ingrese una opción: ");        
            
            switch (op) {
                case 1:
                    System.out.println("Nueva partida");
                    MenuPartida menu1 = new MenuPartida(this.modelo);
                    break;

                case 2:
                    System.out.println("Rankings");
                    MenuRanking menu2 = new MenuRanking(this.modelo);
                    break;

                case 3:
                    System.out.println("Jugadores");
                    MenuJugadores menu3 = new MenuJugadores(this.modelo);
                    break;

                case 0:
                    System.out.println("Adios");
                    break;

                default:
                    System.out.println("Opción incorrecta");
            }
        }
    }
}
